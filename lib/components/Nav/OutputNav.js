"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
// Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

const React = qu4rtet.require("react");

const { Component } = React;
const {
  Menu,
  MenuItem,
  MenuDivider,
  Dialog,
  Button,
  ButtonGroup,
  ContextMenu,
  RadioGroup,
  Radio
} = qu4rtet.require("@blueprintjs/core");
const { TreeNode } = qu4rtet.require("./components/layouts/elements/TreeNode");
const { withRouter } = qu4rtet.require("react-router");
const { connect } = qu4rtet.require("react-redux");
const { FormattedMessage } = qu4rtet.require("react-intl");
const { pluginRegistry } = qu4rtet.require("./plugins/pluginRegistration");

class _AuthenticationNav extends Component {
  constructor(...args) {
    var _temp;

    return _temp = super(...args), this.goTo = path => {
      return this.props.history.push(path);
    }, this.renderContextMenu = () => {
      const { server, serverID, history } = this.props;
      return React.createElement(
        Menu,
        null,
        React.createElement(MenuDivider, { title: server.serverSettingName }),
        React.createElement(MenuDivider, null),
        React.createElement(MenuItem, {
          onClick: this.goTo.bind(this, `/output/${serverID}/add-authentication`),
          text: pluginRegistry.getIntl().formatMessage({
            id: "plugins.output.addAuthentication"
          })
        })
      );
    }, _temp;
  }

  render() {
    const { serverID, server } = this.props;
    return React.createElement(
      TreeNode,
      {
        serverID: serverID,
        childrenNodes: [],
        nodeType: "authentication",
        path: `/output/${this.props.serverID}/authentication`,
        onContextMenu: this.renderContextMenu },
      React.createElement(FormattedMessage, { id: "plugins.output.authenticationNav" })
    );
  }
}

exports._AuthenticationNav = _AuthenticationNav;
class _EndpointsNav extends Component {
  constructor(...args) {
    var _temp2;

    return _temp2 = super(...args), this.goTo = path => {
      return this.props.history.push(path);
    }, this.renderContextMenu = () => {
      const { server, serverID, history } = this.props;
      return React.createElement(
        Menu,
        null,
        React.createElement(MenuDivider, { title: server.serverSettingName }),
        React.createElement(MenuDivider, null),
        React.createElement(MenuItem, {
          onClick: this.goTo.bind(this, `/output/${serverID}/add-endpoint`),
          text: pluginRegistry.getIntl().formatMessage({
            id: "plugins.output.addEndpoint"
          })
        })
      );
    }, _temp2;
  }

  render() {
    const { serverID, server } = this.props;
    return React.createElement(
      TreeNode,
      {
        serverID: serverID,
        nodeType: "endpoints",
        onContextMenu: this.renderContextMenu,
        childrenNodes: [],
        path: `/output/${this.props.serverID}/endpoints` },
      React.createElement(FormattedMessage, { id: "plugins.output.endpointsNav" })
    );
  }
}

exports._EndpointsNav = _EndpointsNav;
class _EPCISOutputNav extends Component {
  constructor(props) {
    super(props);

    this.goTo = path => {
      return this.props.history.push(path);
    };

    this.renderContextMenu = () => {
      const { server, serverID, history } = this.props;
      return React.createElement(
        Menu,
        null,
        React.createElement(MenuDivider, { title: server.serverSettingName }),
        React.createElement(MenuDivider, null),
        React.createElement(MenuItem, {
          onClick: this.goTo.bind(this, `/output/${serverID}/add-criteria`),
          text: pluginRegistry.getIntl().formatMessage({
            id: "plugins.output.addEPCISOutputCriteria"
          })
        })
      );
    };
  }

  render() {
    const { serverID, server } = this.props;
    return React.createElement(
      TreeNode,
      {
        serverID: serverID,
        childrenNodes: [],
        nodeType: "epcis-output-criteria",
        path: `/output/${serverID}/epcis-output-criteria`,
        onContextMenu: this.renderContextMenu },
      React.createElement(FormattedMessage, { id: "plugins.output.EPCISOutputNav" })
    );
  }
}

exports._EPCISOutputNav = _EPCISOutputNav;
class _OutputNavRoot extends Component {
  constructor(...args) {
    var _temp3;

    return _temp3 = super(...args), this.goTo = path => {
      this.props.history.push(path);
    }, this.renderContextMenu = () => {
      const { server, serverID } = this.props;
      return React.createElement(
        Menu,
        null,
        React.createElement(MenuDivider, { title: server.serverSettingName }),
        React.createElement(MenuDivider, null)
      );
    }, _temp3;
  }

  static get PLUGIN_COMPONENT_NAME() {
    return "OutputNavRoot";
  }
  serverHasOutput() {
    return pluginRegistry.getServer(this.props.serverID).appList.includes("output");
  }

  render() {
    const { serverID, server, history } = this.props;

    if (server && this.serverHasOutput()) {
      return React.createElement(
        TreeNode,
        {
          serverID: serverID,
          nodeType: "output",
          onContextMenu: this.renderContextMenu,
          depth: this.props.depth,
          childrenNodes: [React.createElement(_EndpointsNav, {
            key: "EndpointsNav",
            serverID: serverID,
            server: server,
            depth: this.props.depth,
            history: history
          }), React.createElement(_EPCISOutputNav, {
            key: "EPCISOutputNav",
            serverID: serverID,
            server: server,
            depth: this.props.depth,
            history: history
          }), React.createElement(_AuthenticationNav, {
            key: "AuthNav",
            serverID: serverID,
            server: server,
            depth: this.props.depth,
            history: history
          })] },
        React.createElement(FormattedMessage, { id: "plugins.output.navItemsTitle" })
      );
    } else {
      return React.createElement(
        TreeNode,
        { depth: this.props.depth, childrenNodes: [] },
        React.createElement(
          "i",
          null,
          React.createElement(FormattedMessage, { id: "plugins.output.noOutputFound" })
        )
      );
    }
  }
}

exports._OutputNavRoot = _OutputNavRoot;
const OutputNavRoot = exports.OutputNavRoot = connect((state, ownProps) => {
  return {
    server: state.serversettings.servers[ownProps.serverID],
    currentPath: state.layout.currentPath
  };
}, {})(withRouter(_OutputNavRoot));