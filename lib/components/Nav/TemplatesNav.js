"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
// Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

const React = qu4rtet.require("react");

const { Component } = React;
const {
  Menu,
  MenuItem,
  MenuDivider,
  Dialog,
  Button,
  ButtonGroup,
  ContextMenu,
  RadioGroup,
  Radio
} = qu4rtet.require("@blueprintjs/core");
const { TreeNode } = qu4rtet.require("./components/layouts/elements/TreeNode");
const { withRouter } = qu4rtet.require("react-router");
const { connect } = qu4rtet.require("react-redux");
const { FormattedMessage } = qu4rtet.require("react-intl");
const { pluginRegistry } = qu4rtet.require("./plugins/pluginRegistration");

class _TemplatesNav extends Component {
  constructor(...args) {
    var _temp;

    return _temp = super(...args), this.goTo = path => {
      return this.props.history.push(path);
    }, this.renderContextMenu = () => {
      const { server, serverID, history } = this.props;
      return React.createElement(
        Menu,
        null,
        React.createElement(MenuDivider, { title: server.serverSettingName }),
        React.createElement(MenuDivider, null),
        React.createElement(MenuItem, {
          onClick: this.goTo.bind(this, `/templates/${serverID}/add-template`),
          text: pluginRegistry.getIntl().formatMessage({
            id: "plugins.templates.addTemplate"
          })
        })
      );
    }, _temp;
  }

  render() {
    const { serverID, server } = this.props;
    return React.createElement(
      TreeNode,
      {
        serverID: serverID,
        nodeType: "templates",
        onContextMenu: this.renderContextMenu,
        childrenNodes: [],
        path: `/templates/${this.props.serverID}/templates` },
      React.createElement(FormattedMessage, { id: "plugins.templates.templatesNav" })
    );
  }
}

exports._TemplatesNav = _TemplatesNav;
class _TemplatesNavRoot extends Component {
  constructor(...args) {
    var _temp2;

    return _temp2 = super(...args), this.goTo = path => {
      this.props.history.push(path);
    }, this.renderContextMenu = () => {
      const { server, serverID } = this.props;
      return React.createElement(
        Menu,
        null,
        React.createElement(MenuDivider, { title: server.serverSettingName }),
        React.createElement(MenuDivider, null)
      );
    }, _temp2;
  }

  static get PLUGIN_COMPONENT_NAME() {
    return "TemplatesNavRoot";
  }
  serverHasTemplates() {
    return pluginRegistry.getServer(this.props.serverID).appList.includes("templates");
  }

  render() {
    const { serverID, server, history } = this.props;

    if (server && this.serverHasTemplates()) {
      return React.createElement(
        TreeNode,
        {
          serverID: serverID,
          nodeType: "templates",
          onContextMenu: this.renderContextMenu,
          depth: this.props.depth,
          childrenNodes: [React.createElement(_TemplatesNav, {
            key: "TemplatesNav",
            serverID: serverID,
            server: server,
            depth: this.props.depth,
            history: history
          })] },
        React.createElement(FormattedMessage, { id: "plugins.templates.navItemsTitle" })
      );
    } else {
      return React.createElement(
        TreeNode,
        { depth: this.props.depth, childrenNodes: [] },
        React.createElement(
          "i",
          null,
          React.createElement(FormattedMessage, { id: "plugins.templates.noTemplatesFound" })
        )
      );
    }
  }
}

exports._TemplatesNavRoot = _TemplatesNavRoot;
const TemplatesNavRoot = exports.TemplatesNavRoot = connect((state, ownProps) => {
  return {
    server: state.serversettings.servers[ownProps.serverID],
    currentPath: state.layout.currentPath
  };
}, {})(withRouter(_TemplatesNavRoot));